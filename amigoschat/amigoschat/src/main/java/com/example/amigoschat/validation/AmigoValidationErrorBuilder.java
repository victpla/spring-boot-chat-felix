package com.example.amigoschat.validation;
import org.springframework.validation.Errors;
import org.springframework.validation.ObjectError;


public class AmigoValidationErrorBuilder {
    public static AmigoValidationError fromBindingErrors(Errors errors) {
        AmigoValidationError error = new AmigoValidationError("Validation failed. " + errors.getErrorCount() + " error(s)");
        for (ObjectError objectError : errors.getAllErrors()) {
            error.addValidationError(objectError.getDefaultMessage());
        }
        return error;
    }
}