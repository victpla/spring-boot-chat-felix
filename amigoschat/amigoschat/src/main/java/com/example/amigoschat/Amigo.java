package com.example.amigoschat;


import javax.persistence.*;

@Entity
public class Amigo {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private String id;
    private String id_usuario1;
    private String id_usuario2;
    
    /**
     * @return Integer return the id
     */
    public String getId() {
        return id;
    }

    /**
     * @param id the id to set
     */
    public void setId(String id) {
        this.id = id;
    }

    /**
     * @return String return the id_usuario1
     */
    public String getId_usuario1() {
        return id_usuario1;
    }

    /**
     * @param id_usuario1 the id_usuario1 to set
     */
    public void setId_usuario1(String id_usuario1) {
        this.id_usuario1 = id_usuario1;
    }

    /**
     * @return String return the id_usuario2
     */
    public String getId_usuario2() {
        return id_usuario2;
    }

    /**
     * @param id_usuario2 the id_usuario2 to set
     */
    public void setId_usuario2(String id_usuario2) {
        this.id_usuario2 = id_usuario2;
    }



    public Amigo(String idUsuario1, String idUsuario2) {
        super();
        this.id_usuario1 = idUsuario1;
        this.id_usuario2 = idUsuario2;

      }
      public Amigo() {
        super();
      }



}