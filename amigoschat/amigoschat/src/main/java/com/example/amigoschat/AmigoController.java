package com.example.amigoschat;

import java.net.URI;
import java.util.List;

import javax.validation.Valid;

import com.example.amigoschat.Amigo;
import com.example.amigoschat.AmigoRepository;
import com.example.amigoschat.validation.AmigoValidationError;
import com.example.amigoschat.validation.AmigoValidationErrorBuilder;

import org.springframework.http.ResponseEntity;
import org.springframework.jdbc.core.BeanPropertyRowMapper;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.validation.Errors;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

@RestController
@RequestMapping("/api")

public class AmigoController {

    private AmigoRepository repositorio;

    @Autowired
    JdbcTemplate jdbcTemplate;

    @Autowired
    public AmigoController(AmigoRepository repositorio) {
        this.repositorio = repositorio;
    }

    @GetMapping(value = "/listarAmigos")
    public ResponseEntity<Iterable<Amigo>> getAmigos() {

        return ResponseEntity.ok(repositorio.findAll());

    }

    @RequestMapping(value = "/agregarAmigos", method = { RequestMethod.POST, RequestMethod.PUT })
    public ResponseEntity<?> addAmigo(@Valid @RequestBody Amigo amigo, Errors errors) {

        if (errors.hasErrors()) {
            return ResponseEntity.badRequest().body(AmigoValidationErrorBuilder.fromBindingErrors(errors));
        }

        boolean result;

        if (amigo.getId_usuario1().equals(amigo.getId_usuario2())) {

            AmigoValidationError amigoError = new AmigoValidationError("Un usuario no puede ser amigo de si mismo");

            return ResponseEntity.status(402).body(amigoError);

        } else

        {

            if (!encontrarAmistad(amigo.getId_usuario1(), amigo.getId_usuario2())) {

                result = compruebaexistencia(amigo.getId_usuario1());

                if (result) {

                    result = compruebaexistencia(amigo.getId_usuario2());

                }

                if (result) {

                    Amigo amigocreado = repositorio.save(amigo);

                    URI location = ServletUriComponentsBuilder.fromCurrentRequest().path("/{id}")
                            .buildAndExpand(amigocreado.getId()).toUri();

                    return ResponseEntity.created(location).build();

                } else {

                    AmigoValidationError amigoError = new AmigoValidationError("Uno de los usuarios no existe.");

                    return ResponseEntity.status(402).body(amigoError);

                }

            } else {

                AmigoValidationError amigoError = new AmigoValidationError("Ambos usuarios ya son amigos.");

                return ResponseEntity.status(402).body(amigoError);

            }

        }

    }

    public Boolean compruebaexistencia(String identificador) {

        String uri = "http://localhost:7888/api/existeUsuario/";

        uri = uri.concat(identificador);

        RestTemplate restTemplate = new RestTemplate();
        Boolean result = restTemplate.getForObject(uri, Boolean.class);

        return result;

    }

    public Boolean encontrarAmistad(String id1, String id2) {

        List<Amigo> amigoEncontrado;
        String sql = "SELECT * FROM AMIGO where ID_USUARIO1 = ? AND ID_USUARIO2 = ?";

        amigoEncontrado = jdbcTemplate.query(sql, new Object[]{id1, id2},
            new BeanPropertyRowMapper < Amigo > (Amigo.class));

        if (amigoEncontrado.size() > 0 ) {
            
            return true;
            
        } else{

            return false;

        }
    }

}