package com.example.amigoschat;

import org.springframework.data.repository.CrudRepository;
import org.springframework.data.rest.core.config.Projection;

import com.example.amigoschat.Amigo;


public interface AmigoRepository extends CrudRepository<Amigo, String>{
  
  
}


@Projection(name = "NoUsuario2", types = { Amigo.class })
interface NoUsuario2 { 

  String getId_usuario1();
  
}

@Projection(name = "SoloId", types = { Amigo.class })
interface SoloId { 

  String getId();
  
}

