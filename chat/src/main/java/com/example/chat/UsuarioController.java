package com.example.chat;

import java.util.Optional;

import com.example.chat.Usuario;
import com.example.chat.UsuarioRepository;

import org.springframework.http.ResponseEntity;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/api")

public class UsuarioController {

    private UsuarioRepository repositorio;

    @Autowired
    public UsuarioController(UsuarioRepository repositorio) {
        this.repositorio = repositorio;
    }

    @GetMapping(value = "/listarUsuarios")
    public ResponseEntity<Iterable<Usuario>> getUsuarios() {

        return ResponseEntity.ok(repositorio.findAll());

    }

    @GetMapping(value = "/obtenerUsuario/{identificador}")
    public ResponseEntity<Optional<Usuario>> getUsuarioById(@PathVariable String identificador) {
     
        return ResponseEntity.ok(repositorio.findById(identificador));

    }


    @GetMapping(value = "/existeUsuario/{identificador}")
    public boolean existeUsuarioById(@PathVariable String identificador) {
     
        return repositorio.existsById(identificador);

    }


    
}