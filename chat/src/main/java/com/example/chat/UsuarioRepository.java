package com.example.chat;

import org.springframework.data.repository.CrudRepository;
import com.example.chat.Usuario;


public interface UsuarioRepository extends CrudRepository<Usuario, String>{
  
  
}