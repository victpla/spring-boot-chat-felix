package com.example.chat;


import static org.assertj.core.api.Assertions.assertThat;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;

@RunWith(SpringRunner.class)
@SpringBootTest
public class DemoApplicationTests {

	@Autowired
	private UsuarioRepository repo;
	
	@Test
	public void testFindAll() {
		Iterable<Usuario> us = repo.findAll();
		assertThat(us).isNotNull();
	}

	@Test
    public void usuariosTest() throws Exception {
		  Iterable<Usuario> us  = repo.findAll();
		  assertThat(us).doesNotHaveDuplicates();

      }

}
