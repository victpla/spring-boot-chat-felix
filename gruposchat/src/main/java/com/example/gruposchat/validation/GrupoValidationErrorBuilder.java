package com.example.gruposchat.validation;
import org.springframework.validation.Errors;
import org.springframework.validation.ObjectError;


public class GrupoValidationErrorBuilder {
    public static GrupoValidationError fromBindingErrors(Errors errors) {
        GrupoValidationError error = new GrupoValidationError("Validation failed. " + errors.getErrorCount() + " error(s)");
        for (ObjectError objectError : errors.getAllErrors()) {
            error.addValidationError(objectError.getDefaultMessage());
        }
        return error;
    }
}