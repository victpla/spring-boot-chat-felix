package com.example.gruposchat;

import org.springframework.data.repository.CrudRepository;
import org.springframework.data.rest.core.config.Projection;

public interface GrupoRepository extends CrudRepository<Grupo, String>{
  
  
}

@Projection(name = "NoFechaCreacion", types = { Grupo.class })
interface NoFechaCreacion { 

  String getNombre_grupo();
  
}