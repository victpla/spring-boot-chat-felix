package com.example.gruposchat;

import java.time.LocalDateTime;
import java.util.List;

import javax.persistence.*;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;

@Entity
public class Grupo {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private String id;
    
    @NotNull(message = "El nombre del grupo no puede ser null")
	@NotBlank(message = "El nombre del grupo no puede ser vacio")
    private String nombre_grupo;
    
    private String avatar;
    
    @Column(insertable = true, updatable = false)
    private LocalDateTime fechaCreacion;

    @Column(insertable = true, updatable = false)
    private String creadoPor;

    @OneToMany(fetch = FetchType.EAGER, cascade=CascadeType.MERGE, orphanRemoval=true, mappedBy = "grupo")
    private List<UsuariosGrupo> usuarios;

    /**
     * @return Integer return the id
     */
    public String getId() {
        return id;
    }


    /**
     * @param id the id to set
     */
    public void setId(String id) {
        this.id = id;
    }

    /**
     * @return String return the nombre_grupo
     */
    public String getNombre_grupo() {
        return nombre_grupo;
    }

    /**
     * @param nombre_grupo the nombre_grupo to set
     */
    public void setNombre_grupo(String nombre_grupo) {
        this.nombre_grupo = nombre_grupo;
    }

    /**
     * @return String return the fechaCreacion
     */
    public LocalDateTime getFechaCreacion() {
        return fechaCreacion;
    }

    /**
     * @param fechaCreacion the fechaCreacion to set
     */
    public void setFechaCreacion(LocalDateTime fechaCreacion) {
        this.fechaCreacion = fechaCreacion;
    }


    /**
     * @return String return the creadoPor
     */
    public String getCreadoPor() {
        return creadoPor;
    }

    /**
     * @param creadoPor the creadoPor to set
     */
    public void setCreadoPor(String creadoPor) {
        this.creadoPor = creadoPor;
    }


    /**
     * @return String return the avatar
     */
    public String getAvatar() {
        return avatar;
    }

    /**
     * @param avatar the avatar to set
     */
    public void setAvatar(String avatar) {
        this.avatar = avatar;
    }


    public List<UsuariosGrupo> getUsuariosGrupo() { return usuarios; }
    public void setUsuariosGrupo( List<UsuariosGrupo> usuarios) { this.usuarios = usuarios; }
    

    

    @PrePersist
        void onCreate() {
        this.setFechaCreacion(LocalDateTime.now());
        
    }





}