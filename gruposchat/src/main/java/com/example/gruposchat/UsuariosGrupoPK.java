package com.example.gruposchat;


import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Embeddable;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;

import lombok.Data;
import lombok.NoArgsConstructor;

@Embeddable
@Data
@NoArgsConstructor
public class UsuariosGrupoPK implements Serializable{

	/**
	 * 
	 */

	private static final long serialVersionUID = 2678656366948664915L;

	
	@Column(name="ID_GRUPO", insertable = true, updatable = false)
	@NotNull(message = "El grupo al que pertenece un usuario no puede ser null")
	@NotBlank(message = "El grupo al que pertenece un usuario no puede ser vacio")
	private String idGrupo;

	@Column(name="ID_USUARIO", insertable = true, updatable = false)
	@NotNull(message = "El usuario de un grupo no puede ser null")
	@NotBlank(message = "El usuario de un grupo no puede ser vacio")
	private String idUsuario;

	public UsuariosGrupoPK(String idGrupo, String idUsuario) {
		this.idGrupo = idGrupo;
		this.idUsuario = idUsuario;
	}

}
