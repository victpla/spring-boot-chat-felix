package com.example.gruposchat;

import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.MapsId;

@Entity

public class UsuariosGrupo {

    @EmbeddedId
    private UsuariosGrupoPK id;


    @MapsId("idGrupo")
    @JoinColumn(name="ID_GRUPO", referencedColumnName = "ID", insertable = false, updatable = false, nullable = false)
	@ManyToOne(fetch = FetchType.EAGER)
	private Grupo grupo;



    /**
     * @return UsuariosGrupoPK return the id
     */
    public UsuariosGrupoPK getId() {
        return id;
    }

    /**
     * @param id the id to set
     */
    public void setId(UsuariosGrupoPK id) {
        this.id = id;
    }

    /**
     * @return Grupo return the grupo
     */
    public Grupo getGrupo() {
        return grupo;
    }

    /**
     * @param grupo the grupo to set
     */
    public void setGrupo(Grupo grupo) {
        this.grupo = grupo;
    }

}