package com.example.gruposchat;

import java.net.URI;

import javax.validation.Valid;

import com.example.gruposchat.validation.*;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.validation.Errors;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

@RestController
@RequestMapping("/api")

public class GrupoController{

    private GrupoRepository repositorio;
    private UsuariosGrupoRepository repositorioUsuarios;

    @Autowired
    JdbcTemplate jdbcTemplate;

    @Autowired
    public GrupoController(GrupoRepository repositorio, UsuariosGrupoRepository repositorioUsuarios) {
        this.repositorio = repositorio;
        this.repositorioUsuarios = repositorioUsuarios;
    }


    @GetMapping(value = "/listarGrupos")
    public ResponseEntity<Iterable<Grupo>> getGrupos() {

        return ResponseEntity.ok(repositorio.findAll());

    }

    @RequestMapping(value = "/agregarGrupo", method = { RequestMethod.POST, RequestMethod.PUT })
    public ResponseEntity<?> addGrupo(@Valid @RequestBody Grupo grupo, Errors errors) {

        if (errors.hasErrors()) {
            return ResponseEntity.badRequest().body(GrupoValidationErrorBuilder.fromBindingErrors(errors));
        }

        
        
            Grupo grupocreado = repositorio.save(grupo);

            URI location = ServletUriComponentsBuilder.fromCurrentRequest().path("/{id}")
                    .buildAndExpand(grupocreado.getId()).toUri();

            return ResponseEntity.created(location).build();

    }


    @RequestMapping(value = "/aniadirUsuarioGrupo", method = { RequestMethod.POST, RequestMethod.PUT })
	public ResponseEntity<?> aniadirUsuarioGrupo(@Valid @RequestBody UsuariosGrupo usuarioGr, Errors errors) {
		if (errors.hasErrors()) {
			return ResponseEntity.badRequest().body(GrupoValidationErrorBuilder.fromBindingErrors(errors));
		}
        
        boolean result;

        result = true;

		result = compruebaexistencia(usuarioGr.getId().getIdUsuario());
        
        if (result) {


            Grupo grupoAmpliar;
            
            grupoAmpliar = repositorio.findById(usuarioGr.getId().getIdGrupo()).get();
            
            usuarioGr.setGrupo(grupoAmpliar);

            

            grupoAmpliar.getUsuariosGrupo().add(usuarioGr);

            UsuariosGrupo usuarioGrupo = repositorioUsuarios.save(usuarioGr);
            URI location = ServletUriComponentsBuilder.fromCurrentRequest().path("/{idGrupo}")
                    .path("/{idUsuario}").buildAndExpand(usuarioGrupo.getId().getIdGrupo(), usuarioGrupo.getId().getIdUsuario())
                    .toUri();
            return ResponseEntity.created(location).build();
        
        } else {

            GrupoValidationError amigoError = new GrupoValidationError("El usuario indicado no existe.");

            return ResponseEntity.status(402).body(amigoError);

        }

	}
    


    public Boolean compruebaexistencia(String identificador) {

        String uri = "http://localhost:7888/api/existeUsuario/";

        uri = uri.concat(identificador);

        RestTemplate restTemplate = new RestTemplate();
        Boolean result = restTemplate.getForObject(uri, Boolean.class);

        return result;

    }

}